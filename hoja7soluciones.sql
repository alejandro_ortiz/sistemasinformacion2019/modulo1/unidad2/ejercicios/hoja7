﻿USE m1u2h7;

-- 1. Crear una vista que nos permita ver los productos cuyo precio es mayor que 100. Mostrar los campos CODIGO
-- ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA1.
  CREATE OR REPLACE VIEW consulta1 AS
    SELECT p.`CÓDIGO ARTÍCULO`, p.SECCIÓN, p.PRECIO 
      FROM productos p 
      WHERE p.PRECIO>100;

  SELECT * FROM consulta1 c;

-- 2. Modificar la anterior para ordenar por precio de forma descendente
  CREATE OR REPLACE VIEW consulta1ordenada AS
    SELECT p.`CÓDIGO ARTÍCULO`, p.SECCIÓN, p.PRECIO 
      FROM productos p 
      WHERE p.PRECIO>100
      ORDER BY p.PRECIO DESC;

  SELECT * FROM consulta1ordenada c;

-- 3. Crear una vista que utilice como tabla la vista CONSULTA1 y que nos muestre todos productos cuya sección es
-- DEPORTES. Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA2.

  CREATE OR REPLACE VIEW consulta2 AS
    SELECT `c`.`CÓDIGO ARTÍCULO`, c.SECCIÓN, c.PRECIO
      FROM consulta1ordenada c 
      WHERE c.SECCIÓN='deportes';

  SELECT * FROM consulta2 c;

/*  
  5. Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA1.
  a. Código articulo: AR90
  b. Sección: Novedades
  c. Precio: 5;
*/
   INSERT INTO consulta1ordenada VALUE ('AR90', 'NOVEDADES', 5);

/*
  6- Modificar la vista CONSULTA1 y activar el check en la vista en modo local.
  7- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA1.
  a. Código articulo: AR91
  b. Sección: Novedades
  c. Precio: 5;
*/
  INSERT INTO consulta1ordenada VALUE ('AR91', 'NOVEDADES', 5);
  
  -- ¿Qué es lo que ocurre?. Modificar el precio a 110.
  -- NO deja imnsertarlo por la restriccion

  INSERT INTO consulta1ordenada VALUE ('AR91', 'NOVEDADES', 110);

/*
  8- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2.
  a. Código articulo: AR92
  b. Sección: Novedades
  c. Precio: 5;
*/
  INSERT INTO consulta2 VALUE ('AR92', 'NOVEDADES', 5);

/*
  9- Modificar la vista CONSULTA2 y activar el check en la vista en modo local.
  10- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2.
  a. Código articulo: AR93
  b. Sección: Novedades
  c. Precio: 5;
  ¿Qué es lo que ocurre?. Modificar la sección a DEPORTES
*/
  INSERT INTO consulta2 VALUE ('AR93', 'NOVEDADES', 5);
  -- NO deja por el check
  INSERT INTO consulta2 VALUE ('AR93', 'DEPORTES', 5);

/*
  11- Modificar la vista CONSULTA2 y activar el check en la vista en modo CASCADA
  12- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2.
  d. Código articulo: AR94
  e. Sección: DEPORTES
  f. Precio: 5;
  ¿Qué es lo que ocurre?. Modificar el precio a 200
*/

  INSERT INTO consulta2 VALUE ('AR94', 'DEPORTES', 5);
  -- No deja por el check
  INSERT INTO consulta2 VALUE ('AR94', 'DEPORTES', 200);
